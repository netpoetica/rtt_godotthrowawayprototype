extends Node2D

var Asset = load("res://Asset.gd")

export(Resource) var asset
export(Texture) var texture

var dragging: bool = false
var mouse_in: bool = false
var hover_rect: ColorRect = null
var texture_rect: TextureRect = null
var map_viewport_texture_rect: TextureRect = null

func _ready():
	get_node("/root/GameRoot").emit_signal("placed_object_placed", self.get_instance_id(), asset)
	
	map_viewport_texture_rect = get_node("/root/GameRoot/VBoxContainer/BodyMarginContainer/MapViewportContainer/MapViewport/TextureRect")
	hover_rect = get_node("RTT_MapCameraTogglePanel/HoverColorRect")
	
	texture_rect = get_node("RTT_MapCameraTogglePanel/TextureRect")
	texture_rect.texture = texture
	texture_rect.connect(
		"mouse_entered",
		self,
		"on_mouse_entered"
	)
	texture_rect.connect(
		"mouse_exited",
		self,
		"on_mouse_exited"
	)

func _input(event):
	if event is InputEventMouseButton:
		if !event.pressed && dragging:
			dragging = false
		elif event.pressed && mouse_in:
			dragging = true
	if event is InputEventMouseMotion and dragging:
		self.position = map_viewport_texture_rect.get_local_mouse_position() - Vector2(texture.get_width() / 2, texture.get_height() / 2)

func on_mouse_entered():
	mouse_in = true
	if !hover_rect.visible:
		# print("Hovering placed object...")
		hover_rect.visible = true

func on_mouse_exited():
	mouse_in = false
	if hover_rect.visible:
		# print("Stop overing placed object...")
		hover_rect.visible = false
