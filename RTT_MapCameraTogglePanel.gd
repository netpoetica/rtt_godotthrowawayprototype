extends Panel

var map_viewport_camera: Camera2D = null

func _ready():
	var root = get_node('/root')
	var map_viewport = get_node('/root/GameRoot/VBoxContainer/BodyMarginContainer/MapViewportContainer/MapViewport')
	map_viewport_camera = map_viewport.get_child(0)
	connect(
		'mouse_entered',
		self,
		'_on_RTT_MapCameraTogglePanel_mouse_entered'
	)
	connect(
		'mouse_exited',
		self,
		'_on_RTT_MapCameraTogglePanel_mouse_exited'
	)

func _on_RTT_MapCameraTogglePanel_mouse_entered():
	print('Disabling map camera...')
	map_viewport_camera.clear_current()

func _on_RTT_MapCameraTogglePanel_mouse_exited():
	print('Re-enabling map camera...')
	map_viewport_camera.make_current()
