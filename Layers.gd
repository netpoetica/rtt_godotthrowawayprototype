extends Tree

var dragging = false
var selected_item: TreeItem = null
var selected_item_parent: TreeItem = null
var preview: Label = null
var root: TreeItem = null
var layers: TreeItem = null
var delete_texture: Texture = load("res://asset-lib-icons/ui-icons/garbage-can.png")

func _ready():
	# root will not be visible.
	root = self.create_item()
	root.set_text(0, "ROOT")
	layers = self.create_item(root)
	layers.set_text(0, "Layers")
	
	get_node("/root/GameRoot").connect("placed_object_placed", self, "on_placed_object_placed")

func on_placed_object_placed(id: int, asset):
	var item = self.create_item(layers)
	item.set_text(0, asset.asset_name)
	item.set_icon(0, delete_texture)
	item.set_meta("instance_id", asset.id)

func _on_Tree_cell_selected():
	print("selecting item")
	selected_item = self.get_selected()
	if selected_item != layers:
		selected_item_parent = selected_item.get_parent()
		selected_item.get_parent().remove_child(selected_item)
		preview = Label.new()
		preview.text = selected_item.get_text(0)
		preview.set_position(get_global_mouse_position())
		add_child(preview)
	else:
		selected_item = null

func _input(event):
	if event is InputEventMouseButton:
		if !event.pressed and selected_item:
			print("release item")
			remove_child(preview)
			var new_child = self.create_item(selected_item_parent)
			new_child.set_text(0, preview.text)
			preview = null
			selected_item = null
			selected_item_parent = null
			dragging = false
		else:
			dragging = true

	if event is InputEventMouseMotion:
		# set_process_input(false)
		if selected_item and dragging:
			print("dragging preview item")
			var position = get_global_mouse_position()
			preview.set_position(position)
