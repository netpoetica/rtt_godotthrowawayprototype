extends VBoxContainer

var Asset = load("res://Asset.gd")
var placed_object = load('res://RTT_PlacedObject.tscn')

var selected_item: TextureRect = null
var dragging = false
var drag_start_camera_transform = null
var map_viewport: Viewport = null
var map_viewport_camera: Camera2D = null
var map_viewport_texture_rect: TextureRect = null
var map_viewport_texture: Texture = null
var mouse_in: bool = false
var root: Viewport = null

export(Resource) var asset = Asset.new()

var asset_label_node: Label = null
var asset_texture_rect_node: TextureRect = null

func _ready():
	root = get_node('/root')
	map_viewport = get_node('/root/GameRoot/VBoxContainer/BodyMarginContainer/MapViewportContainer/MapViewport')
	map_viewport_camera = map_viewport.get_child(0)
	map_viewport_camera.position = Vector2(0, 0)
	# print("map_viewport_camera position", map_viewport_camera.position)
	map_viewport_texture_rect = map_viewport.get_child(1)
	map_viewport_texture = map_viewport_texture_rect.texture
	# map_viewport_texture_rect.position = Vector2(0, 0)
	# print("map_viewport_texture_rect position", map_viewport_texture_rect.position)
	
	asset_label_node = get_node("AssetLabel")
	asset_texture_rect_node = get_node("AssetTextureRect")
	asset_label_node.text = asset.asset_name
	var texture: Texture = load("res://" + asset.asset_path)
	asset_texture_rect_node.texture = texture

func _input(event):
	if event is InputEventMouseButton:
		if !event.pressed && selected_item:
			# make the map camera current again
			map_viewport_camera.make_current()
			
			# print("release grid item, event position", event.position)
			# print("map_viewport_camera position", map_viewport_camera.position)
			# stop dragging
			dragging = false
			# remove the temporary object
			root.remove_child(selected_item)
			
			# create a new placed object instance
			var placed_object_instance = placed_object.instance()
			placed_object_instance.asset = asset
			placed_object_instance.texture = selected_item.texture

			# determine the position on the map to place it
			var global_mouse_pos = get_global_mouse_position()
			# print("global mouse pos", global_mouse_pos)
			var viewport_mouse_pos = map_viewport.get_mouse_position()
			# print("map viewport mouse mos", viewport_mouse_pos)
			var camera_local_mouse_position = map_viewport_camera.get_local_mouse_position()
			# print("camera local mouse position", camera_local_mouse_position)
			var map_viewport_texture_rect_position = map_viewport_texture_rect.get_local_mouse_position()
			# print("map texture local mouse position", map_viewport_texture_rect_position)
			var position_centered = map_viewport_texture_rect_position - (.5 * selected_item.texture.get_size())
			placed_object_instance.position = position_centered
			# print("place object position", placed_object_instance.position)
			# placed_object_instance.scale = map_viewport_camera.scale
			map_viewport.add_child(placed_object_instance)
			selected_item = null

		
			# reset it back to start camera transform because for some reason it stops
			map_viewport_camera.transform = drag_start_camera_transform
		elif event.pressed && mouse_in:
			# print("select grid item")
			dragging = true
			drag_start_camera_transform = map_viewport_camera.transform
			map_viewport_camera.clear_current()
			var selected = self.get_child(1)
			selected_item = TextureRect.new()
			selected_item.texture = selected.texture
			selected_item.mouse_filter = 1
			root.add_child(selected_item)
			selected_item.set_position(event.position - (.5 * selected_item.texture.get_size()))
	if event is InputEventMouseMotion:
		if selected_item and dragging:
			if map_viewport_camera.current:
				map_viewport_camera.clear_current()
			# print("dragging grid item")
			# var position = get_global_mouse_position()
			selected_item.set_position(event.position - (.5 * selected_item.texture.get_size()))

func _on_GridItem1_mouse_entered():
	# print('mouse in')
	mouse_in = true

func _on_GridItem1_mouse_exited():
	# print('mouse out')
	mouse_in = false
