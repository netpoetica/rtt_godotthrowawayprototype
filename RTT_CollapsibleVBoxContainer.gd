extends VBoxContainer

export(String) var label_text = "Label"

func _ready():
	var check_button: CheckButton = self.get_node("CheckButton")
	check_button.text = label_text

func _on_CheckButton_toggled(_button_pressed):
	var panel: Panel = self.get_node("Panel")
	panel.visible = !panel.visible
