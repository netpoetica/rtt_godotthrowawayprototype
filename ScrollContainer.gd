extends ScrollContainer

var map_viewport: Viewport = null
var map_viewport_camera: Camera2D = null
var timer: SceneTreeTimer = null

func _ready():
	var root = get_node('/root')
	map_viewport = get_node('/root/GameRoot/VBoxContainer/BodyMarginContainer/MapViewportContainer/MapViewport')
	map_viewport_camera = map_viewport.get_child(0)
	get_v_scrollbar().connect('scrolling', self, 'disable_map_camera')

func disable_map_camera():
	if map_viewport_camera.current && !timer:
		print('Disabling map camera during scroll...')
		timer = get_tree().create_timer(.5)
		timer.connect("timeout", self, "clear_scrollbar_dragging")
		map_viewport_camera.clear_current()
		map_viewport.stop_drag()

func clear_scrollbar_dragging():
	print("clearing scrollbar dragging")
	timer = null
	map_viewport_camera.make_current()

func _on_ScrollContainer_focus_exited():
	print("Scroll container focus exited...")
	map_viewport_camera.make_current()
