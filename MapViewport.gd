extends Viewport

const ZOOM_SPEED: float = 0.1
var zoom_vec: Vector2 = Vector2(ZOOM_SPEED, ZOOM_SPEED)
var camera: Camera2D = null
var dragging: bool = false
var drag_position: Vector2 = Vector2.ONE
var texture_rect: TextureRect = null

func _ready():
	camera = self.get_node("Camera2D")
	texture_rect = self.get_node("TextureRect")

func stop_drag():
	print("Stop map dragging...")
	dragging = false

func _input(event):
	if camera.is_current():
		if event is InputEventMouseButton:
			# print("mouse motion position in map", event.position)
			# print("map camera position", camera.position)
			# print("map camera zoom", camera.zoom)
			if event.button_index == BUTTON_WHEEL_UP:
				camera.zoom += zoom_vec
			elif event.button_index == BUTTON_WHEEL_DOWN:
				camera.zoom -= zoom_vec
			print('zoom', camera.zoom)
			if event.pressed:
				print("Enabling map drag boolean...")
				drag_position = event.position
				dragging = true
			else:
				print("Disabling map drag boolean...")
				dragging = false
		elif event is InputEventMouseMotion and dragging:
			# print('Moving map...')
			camera.position += (drag_position - event.position)
			drag_position = event.position


func _on_VScrollBar_value_changed(value):
	print("zoomscrollbar value", value)
	camera.zoom = Vector2(value, value)
	pass # Replace with function body.
