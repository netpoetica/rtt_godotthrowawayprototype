extends Node2D

var Asset = preload("res://Asset.gd")
var AssetGridItem = preload("res://AssetGridItem.tscn")

signal placed_object_placed(instance_id, asset)
signal placed_object_removed(instance_id)

var map_viewport: Viewport = null

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()

func on_window_resize():
	# print("SCREEN RESIZED")
	var screen_size = OS.get_window_size()
	var screen_rect = Rect2(Vector2(0, 0), screen_size)
	get_viewport().set_attach_to_screen_rect(screen_rect)
	map_viewport.get_viewport().set_attach_to_screen_rect(screen_rect)

func _ready():
	map_viewport = get_node('/root/GameRoot/VBoxContainer/BodyMarginContainer/MapViewportContainer/MapViewport')
	get_viewport().connect("size_changed", self, "on_window_resize")
	
	var assets = []
	var file_paths = [
		"asset-lib-icons/Chest.png",
		"asset-lib-icons/Chicken Crate.png",
		"asset-lib-icons/Crate.png",
		"asset-lib-icons/Crate 2.png",
		"asset-lib-icons/Empty Crate.png",
		"asset-lib-icons/Empty Crate 2.png",
		"asset-lib-icons/Metal Box.png",
		"asset-lib-icons/Metal Crate 2.png",
		"asset-lib-icons/Tomato Crate.png"
	]

	for i in range(file_paths.size()):
		var path = file_paths[i]
		# get rid of file type
		var asset_name = path.substr(0, path.length() - 4)
		asset_name = asset_name.replace("asset-lib-icons/", "")
		# print(asset_name)
		var asset = Asset.new()
		asset.setup(
			path,
			asset_name,
			0,
			["object"]
		)
		assets.append(asset)
	
	var monster_file_paths = [
		"asset-lib-icons/tokens/monsters/Dire Wolf.png",
		"asset-lib-icons/tokens/monsters/Gnoll Warrior.png",
		"asset-lib-icons/tokens/monsters/Mummy Lord.png",
	]

	for i in range(monster_file_paths.size()):
		var path = monster_file_paths[i]
		# get rid of file type
		var asset_name = path.substr(0, path.length() - 4)
		asset_name = asset_name.replace("asset-lib-icons/tokens/monsters/", "")
		# print(asset_name)
		var asset = Asset.new()
		asset.setup(
			path,
			asset_name,
			0,
			["monster"]
		)
		assets.append(asset)
	
	var npc_file_paths = [
		"asset-lib-icons/tokens/npcs/bard.png",
		"asset-lib-icons/tokens/npcs/druid.png",
		"asset-lib-icons/tokens/npcs/dwarf.png",
		"asset-lib-icons/tokens/npcs/knight.png",
		"asset-lib-icons/tokens/npcs/minotaur.png",
		"asset-lib-icons/tokens/npcs/wizard.png",
	]

	for i in range(npc_file_paths.size()):
		var path = npc_file_paths[i]
		# get rid of file type
		var asset_name = path.substr(0, path.length() - 4)
		asset_name = asset_name.replace("asset-lib-icons/tokens/npcs/", "")
		# print(asset_name)
		var asset = Asset.new()
		asset.setup(
			path,
			asset_name,
			0,
			["npc"]
		)
		assets.append(asset)
	
	var asset_lib: GridContainer = get_node(
		"/root/GameRoot/VBoxContainer/BodyMarginContainer/GridContainer/AssetLayer__RTT_MapCameraTogglePanel/VBoxContainer/Panel/VBoxContainer/ScrollContainer/GridContainer"
	)
	
	for j in range(assets.size()):
		var asset = assets[j]
		var asset_grid_item = AssetGridItem.instance()
		asset_grid_item.asset = asset
		asset_lib.add_child(asset_grid_item)
