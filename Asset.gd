extends Node

class_name Asset

enum ASSET_TYPE {
	Unknown,
	Map,
	PlacedObject,
	Token,
}

const tags = [
	"object",
	"monster",
	"npc",
	"player",
	"map"
]

export(int) var id = null
export(String) var asset_name: String = ""
export(ASSET_TYPE) var asset_type = ASSET_TYPE.Unknown
export(Array) var asset_tags: Array = []
export(String) var asset_path: String = ""

func _init():
	id = get_instance_id()

func setup(_asset_path, _asset_name, _asset_type, _asset_tags):
	asset_path = _asset_path
	asset_name = _asset_name
	asset_type = _asset_type
	asset_tags = _asset_tags
