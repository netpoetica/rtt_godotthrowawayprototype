extends TextureButton

func _on_TextureButton_pressed():
	var filters_panel: Panel = get_node('/root/GameRoot/VBoxContainer/BodyMarginContainer/GridContainer/AssetLayer__RTT_MapCameraTogglePanel/VBoxContainer/Panel/VBoxContainer/FiltersPanel')
	filters_panel.visible = !filters_panel.visible
